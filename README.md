# ThengOS Logos
#### Formats

* svg
* png

#### Tools
###### Artwork

* Inkscpae

#### 3D
* Blender

#### Image size

 Image                                  | Starndard Size | File Name                             |
| :-------------------------------------|:--------------| :-------------------------------------|
| Main Logo No Background               | 512 x 512 px   |  theng os Final Logo-No BG.svg        |
| Main Logo with text No BG             | 545 x 204 px   |  theng os Intro.svg       |
| Main Logo with text transparant white | 545 x 204 px   |  theng os Final Logo Only White 50% Logo.svg |
| Main Logo with text transparant Black | 545 x 204 px   |  theng os Final Logo Only Black 50 percentage Logo.svg |


#### 3D processing

* Use blender 17.9 and above
* Wall paper resolution  :4k
* Render Samples required minimum : 35
* file : ThenggOs logo.blend
* Logos and Images for thengOS GNU/Linux Distro